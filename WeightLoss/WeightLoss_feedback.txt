09/24/19 14:43

Problem 1 (5 points):
Score += 5

Problem 2 (5 points):
Score += 5

Problem 3 (10 points):
Score += 10

Problem 4 (10 points):
there should be two plots here; one for each of the parameter sets, with each initial condition on each. I don't know what this plot is of. Also, make sure you plot the fixed points for the system.
Score += 2

Problems 5 and 6 (10 points):
Score += 0

Problem 7 (5 points):
Score += 0

Code Quality (5 points):
Score += 5

Total score: 27/50 = 54.0%

-------------------------------------------------------------------------------

09/26/19 17:10

Problem 1 (5 points):
Score += 5

Problem 2 (5 points):
Score += 5

Problem 3 (10 points):
Score += 10

Problem 4 (10 points):
Score += 10

Problems 5 and 6 (10 points):
Score += 10

Problem 7 (5 points):
Score += 5

Code Quality (5 points):
Score += 5

Extra Credit (10 points):
Score += 0

Total score: 50/60 = 83.33%

-------------------------------------------------------------------------------

