# opengym.py
"""Volume 2: Open Gym
<Name>
<Class>
<Date>
"""

import gym
import numpy as np
from IPython.display import clear_output
import random
import time

def find_qvalues(env,alpha=.1,gamma=.6,epsilon=.1):
    """
    Use the Q-learning algorithm to find qvalues.

    Parameters:
        env (str): environment name
        alpha (float): learning rate
        gamma (float): discount factor
        epsilon (float): maximum value

    Returns:
        q_table (ndarray nxm)
    """
    # Make environment
    env = gym.make(env)
    # Make Q-table
    q_table = np.zeros((env.observation_space.n,env.action_space.n))

    # Train
    for i in range(1,100001):
        # Reset state
        state = env.reset()

        epochs, penalties, reward, = 0,0,0
        done = False

        while not done:
            # Accept based on alpha
            if random.uniform(0,1) < epsilon:
                action = env.action_space.sample()
            else:
                action = np.argmax(q_table[state])

            # Take action
            next_state, reward, done, info = env.step(action)

            # Calculate new qvalue
            old_value = q_table[state,action]
            next_max = np.max(q_table[next_state])

            new_value = (1-alpha) * old_value + alpha * (reward + gamma * next_max)
            q_table[state, action] = new_value

            # Check if penalty is made
            if reward == -10:
                penalties += 1

            # Get next observation
            state = next_state
            epochs += 1

        # Print episode number
        if i % 100 == 0:
            clear_output(wait=True)
            print(f"Episode: {i}")

    print("Training finished.")
    return q_table

# Problem 1
def random_blackjack(n):
    """
    Play a random game of Blackjack. Determine the
    percentage the player wins out of n times.

    Parameters:
        n (int): number of iterations

    Returns:
        percent (float): percentage that the player
                         wins
    """
    env = gym.make("Blackjack-v0")
    # Track the win/loss percentage
    wins = 0

    for _ in range(n):
        env.reset()
        end = False
        while(not end):
            outcome = env.step(env.action_space.sample())
            end = outcome[2]
            if end:
                if outcome[1]>0:
                    wins += 1

    return wins / n

# Problem 2
def blackjack(n=11):
    """
    Play blackjack with naive algorithm.

    Parameters:
        n (int): maximum accepted player hand

    Return:
        reward (int): total reward
    """
    RUNS = 10000

    # Initialize the environment
    env = gym.make("Blackjack-v0")

    # Track wins
    reward = 0

    for i in range(RUNS):
        state = env.reset()
        end = False
        while state[0] <= n:
            # hit
            observation = env.step(1)
            state = observation[0]
            reward += observation[1]
        # stay
        final = env.step(0)
        reward += final[1]

    return reward / RUNS

    # I --think-- that the best is in the 17 range,
    # but they all give similar percentages, average reward of -.01

# Problem 3
def cartpole():
    """
    Solve CartPole-v0 by checking the velocity
    of the tip of the pole

    Return:
        time (float): time cartpole is held vertical
    """
    env = gym.make("CartPole-v0")

    start = time.time()
    try:
        state = env.reset()
        done = False
        while not done:
            # Render the environment
            env.render()
            # Catch the important stuff--state and if the sim is terminated
            # If the velocity is positive, push cart positive, etc.
            state, _ , done, _ = env.step(1 if state[3] > 0 else 0)
            if done:
                break
    finally:
        env.close()

    return time.time() - start


# Problem 4
def car():
    """
    Solve MountainCar-v0 by checking the position
    of the car.

    Return:
        time (float): time to solve environment
    """
    # Initialize the environment
    env = gym.make("MountainCar-v0")

    start = time.time()

    try:
        prev_state = env.reset()
        state = env.step(2)[0]
        done = False
        while True:
            env.render()
            dx = state[0] - prev_state[0]
            prev_state = state
            obs = env.step(2 if dx > 0 else 0)
            if obs[2]:
                break
            state = obs[0]
    finally:
        env.close()

    return time.time() - start



# Problem 5
def taxi(q_table, iters = 10000):
    """
    Compare naive and q-learning algorithms.

    Parameters:
        q_table (ndarray nxm): table of qvalues

    Returns:
        naive (int): reward of naive algorithm
        q_reward (int): reward of Q-learning algorithm
    """
    ITERATIONS = iters

    # Initialize environment
    env = gym.make("Taxi-v2")

    naive_rewards = 0
    for _ in range(ITERATIONS):

        # Random actions
        env.reset()
        done = False
        while not done:
            _,reward,done,_ = env.step(env.action_space.sample())
            naive_rewards += reward


    # Now run with q values
    q_rewards = 0
    for _ in range(ITERATIONS):

        state = env.reset()
        done = False
        while not done:
            action = np.argmax(q_table[state])
            state, q_table_reward, done, _ = env.step(action)
            q_rewards += q_table_reward

    env.close()

    return naive_rewards / ITERATIONS, q_rewards / ITERATIONS
